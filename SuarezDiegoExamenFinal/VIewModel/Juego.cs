﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SuarezDiegoExamenFinal.VIewModel
{
    public class Juego
    {
        public string[] player1Hand { get; set; }
        public string[] player2Hand { get; set; }
        public string[] player3Hand { get; set; }
        public string[] player4Hand { get; set; }
        public string[] player5Hand { get; set; }
        public string[] cpuHand { get; set; }
        public int result { get; set; }
        public string playerResult { get; set; }
        public string cpuResult { get; set; }
    }
}
