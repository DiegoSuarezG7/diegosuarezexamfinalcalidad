﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SuarezDiegoExamenFinal.Models;
using SuarezDiegoExamenFinal.Service;
using SuarezDiegoExamenFinal.VIewModel;

namespace SuarezDiegoExamenFinal.Controllers
{
    public class HomeController : Controller
    {
        private readonly IRepartoService repartoService;
        private readonly IJuegoService juegoService;
        public HomeController(IRepartoService repartoService, IJuegoService juegoService)
        {
            this.repartoService = repartoService;
            this.juegoService = juegoService;
        }
        public IActionResult Index()
        {
            return View();
        }


        [HttpGet]
        public IActionResult Juego()
        {      
            RepartoDeCartas dc = new RepartoDeCartas();
            dc.Reparto();

            Juego data = new Juego
            {
                player1Hand = dc.RenameHands(dc.sortedPlayer1Hand),
                player2Hand = dc.RenameHands(dc.sortedPlayer2Hand),
                player3Hand = dc.RenameHands(dc.sortedPlayer3Hand),
                player4Hand = dc.RenameHands(dc.sortedPlayer4Hand),
                player5Hand = dc.RenameHands(dc.sortedPlayer5Hand),

                result = dc.result
            };

            return View(data);
        }

        

    }
}
