﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SuarezDiegoExamenFinal.Models
{
	public class Carta
	{
        public enum Suit
		{
			Treboles,
			Diamantes,
			Corazones,
			Espadas
		}

		public enum Value
		{
			As,
			dos,
			tres,
			cuatro,
			cinco,
			seis,
			Siete,
			ocho,
			nueve,
			dies,
			once,
			Reina,
			Rey
		}

	
		public Value myValue { get; set; }

		public Suit mySuit { get; set; }

		public Carta()
		{
			
		}
		public Carta(Suit myValue, Value mySuit)
		{
            this.myValue = (Value)myValue;
            this.mySuit = (Suit)mySuit;
		}
	}
}
