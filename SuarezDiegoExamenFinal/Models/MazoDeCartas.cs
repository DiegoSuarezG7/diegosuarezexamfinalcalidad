﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SuarezDiegoExamenFinal.Models
{
    public class MazoDeCartas : Carta
    {
        const int NumberOfCards = 52; 
        private Carta[] Deck; 
        public MazoDeCartas()
        {
            Deck = new Carta[NumberOfCards];
        }

        public Carta[] getDeck { get { return Deck; } } 

        public void SetupDeck()
        {
            int i = 0;
            foreach (Suit s in Enum.GetValues(typeof(Suit)))
            {
                foreach (Value v in Enum.GetValues(typeof(Value)))
                {
                    Deck[i] = new Carta { mySuit = s, myValue = v };
                    i++;
                }
            }
            ShuffleCards();
        }
        
        public void ShuffleCards()
        {
            Random rand = new Random();
            Carta temp;
            
            for (int shuffle = 0; shuffle < 1000; shuffle++)
            {
                for (int i = 0; i < NumberOfCards; i++)
                {
                    
                    int SecondCardIndex = rand.Next(13);
                    temp = Deck[i];
                    Deck[i] = Deck[SecondCardIndex];
                    Deck[SecondCardIndex] = temp;
                }
            }
        }
    }
}
