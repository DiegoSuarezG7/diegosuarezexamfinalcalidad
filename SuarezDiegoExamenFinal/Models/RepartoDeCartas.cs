﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SuarezDiegoExamenFinal.Models
{
    public class RepartoDeCartas : MazoDeCartas
    {
        public Carta[] player1Hand;
        public Carta[] player2Hand;
        public Carta[] player3Hand;
        public Carta[] player4Hand;
        public Carta[] player5Hand;
        public Carta[] sortedPlayer1Hand;
        public Carta[] sortedPlayer2Hand;
        public Carta[] sortedPlayer3Hand;
        public Carta[] sortedPlayer4Hand;
        public Carta[] sortedPlayer5Hand;

        public int result;
        public Hand winningPlayerHand;
        public Hand winningCpuHand;
        public RepartoDeCartas()
        {
            player1Hand = new Carta[5];
            player2Hand = new Carta[5];
            player3Hand = new Carta[5];
            player4Hand = new Carta[5];
            player5Hand = new Carta[5];
            sortedPlayer1Hand = new Carta[5];
            sortedPlayer2Hand = new Carta[5];
            sortedPlayer3Hand = new Carta[5];
            sortedPlayer4Hand = new Carta[5];
            sortedPlayer5Hand = new Carta[5];

        }

        public void Reparto()
        {
            SetupDeck(); 
            getHand(); 
            sortCards(); 
            EvaluateHands(); 

        }

        public void getHand()
        {
            
            for (int i = 0; i < 5; i++)
                player1Hand[i] = getDeck[i];

            
            for (int i = 5; i < 10; i++)
                player2Hand[i - 5] = getDeck[i];

            for (int i = 10; i < 15; i++)
                player3Hand[i - 10] = getDeck[i];

            for (int i = 15; i < 20; i++)
                player4Hand[i - 15] = getDeck[i];

            for (int i = 20; i < 25; i++)
                player5Hand[i - 20] = getDeck[i];
        }

        public void sortCards()
        {
            var queryPlayer1 = from hand in player1Hand
                              orderby hand.myValue
                              select hand;

            var queryPlayer2 = from hand in player2Hand
                              orderby hand.myValue
                              select hand;

            var queryPlayer3 = from hand in player3Hand
                              orderby hand.myValue
                              select hand;

            var queryPlayer4 = from hand in player4Hand
                              orderby hand.myValue
                              select hand;

            var queryPlayer5 = from hand in player5Hand
                              orderby hand.myValue
                              select hand;



            var index = 0;
            foreach (var element in queryPlayer1.ToList())
            {
                sortedPlayer1Hand[index] = element;
                index++;
            }

            index = 0;
            foreach (var element in queryPlayer2.ToList())
            {
                sortedPlayer2Hand[index] = element;
                index++;
            }

            index = 0;
            foreach (var element in queryPlayer3.ToList())
            {
                sortedPlayer3Hand[index] = element;
                index++;
            }

            index = 0;
            foreach (var element in queryPlayer4.ToList())
            {
                sortedPlayer4Hand[index] = element;
                index++;
            }

            index = 0;
            foreach (var element in queryPlayer5.ToList())
            {
                sortedPlayer5Hand[index] = element;
                index++;
            }
        }

        public string[] RenameHands(Carta[] hand)
        {
            string[] newHand = new string[hand.Length];
            int i = 0;
            foreach (var card in hand)
            {
                newHand[i] = (Convert.ToString(card.myValue)) + " de " + (Convert.ToString(card.mySuit));

                i++;
            }
            return newHand;
        }

        public void EvaluateHands()
        {
            EvaluarMano player1HandEvaluator = new EvaluarMano(sortedPlayer1Hand);
            EvaluarMano player2HandEvaluator = new EvaluarMano(sortedPlayer2Hand);
            EvaluarMano player3HandEvaluator = new EvaluarMano(sortedPlayer3Hand);
            EvaluarMano player4HandEvaluator = new EvaluarMano(sortedPlayer4Hand);
            EvaluarMano player5HandEvaluator = new EvaluarMano(sortedPlayer5Hand);

            Hand playerHand1 = player1HandEvaluator.EvaluateHand();
            Hand playerHand2 = player2HandEvaluator.EvaluateHand();
            Hand playerHand3 = player3HandEvaluator.EvaluateHand();
            Hand playerHand4 = player4HandEvaluator.EvaluateHand();
            Hand playerHand5 = player5HandEvaluator.EvaluateHand();


            if (playerHand1 > playerHand2 && playerHand1 > playerHand3 && playerHand1 > playerHand4 && playerHand1 > playerHand5)
            {
                result = 0;

            }
            else if (playerHand2 > playerHand1 && playerHand2 > playerHand3 && playerHand2 > playerHand4 && playerHand2 > playerHand5)
            {
                result = 1;

            }
            else if (playerHand3 > playerHand1 && playerHand3 > playerHand2 && playerHand3 > playerHand4 && playerHand3 > playerHand5)
            {
                result = 2;

            }
            else if (playerHand4 > playerHand1 && playerHand4 > playerHand2 && playerHand4 > playerHand3 && playerHand4 > playerHand5)
            {
                result = 3;

            }
            else if (playerHand5 > playerHand1 && playerHand5 > playerHand2 && playerHand5 > playerHand3 && playerHand5 > playerHand4)
            {
                result = 4;

            }
            else
            {
                if (player1HandEvaluator.HandValues.Total > player2HandEvaluator.HandValues.Total && player1HandEvaluator.HandValues.Total > player3HandEvaluator.HandValues.Total && player1HandEvaluator.HandValues.Total > player4HandEvaluator.HandValues.Total && player1HandEvaluator.HandValues.Total > player5HandEvaluator.HandValues.Total)
                {
                    result = 0;

                }
                else if (player2HandEvaluator.HandValues.Total > player1HandEvaluator.HandValues.Total && player2HandEvaluator.HandValues.Total > player3HandEvaluator.HandValues.Total && player2HandEvaluator.HandValues.Total > player4HandEvaluator.HandValues.Total && player2HandEvaluator.HandValues.Total > player5HandEvaluator.HandValues.Total)
                {
                    result = 1;

                }
                else if (player3HandEvaluator.HandValues.Total > player1HandEvaluator.HandValues.Total && player3HandEvaluator.HandValues.Total > player2HandEvaluator.HandValues.Total && player3HandEvaluator.HandValues.Total > player4HandEvaluator.HandValues.Total && player3HandEvaluator.HandValues.Total > player5HandEvaluator.HandValues.Total)
                {
                    result = 2;

                }
                else if (player4HandEvaluator.HandValues.Total > player1HandEvaluator.HandValues.Total && player4HandEvaluator.HandValues.Total > player2HandEvaluator.HandValues.Total && player4HandEvaluator.HandValues.Total > player3HandEvaluator.HandValues.Total && player4HandEvaluator.HandValues.Total > player5HandEvaluator.HandValues.Total)
                {
                    result = 3;

                }
                else if (player5HandEvaluator.HandValues.Total > player1HandEvaluator.HandValues.Total && player5HandEvaluator.HandValues.Total > player2HandEvaluator.HandValues.Total && player5HandEvaluator.HandValues.Total > player3HandEvaluator.HandValues.Total && player5HandEvaluator.HandValues.Total > player4HandEvaluator.HandValues.Total)
                {
                    result = 4;

                }
                
                else if (player1HandEvaluator.HandValues.Total > player2HandEvaluator.HandValues.Total && player1HandEvaluator.HandValues.Total > player3HandEvaluator.HandValues.Total && player1HandEvaluator.HandValues.Total > player4HandEvaluator.HandValues.Total && player1HandEvaluator.HandValues.Total > player5HandEvaluator.HandValues.Total)
                {
                    result = 0;

                }
                else if (player2HandEvaluator.HandValues.Total > player1HandEvaluator.HandValues.Total && player2HandEvaluator.HandValues.Total > player3HandEvaluator.HandValues.Total && player2HandEvaluator.HandValues.Total > player4HandEvaluator.HandValues.Total && player2HandEvaluator.HandValues.Total > player5HandEvaluator.HandValues.Total)
                {
                    result = 1;

                }
                else if (player3HandEvaluator.HandValues.Total > player1HandEvaluator.HandValues.Total && player3HandEvaluator.HandValues.Total > player2HandEvaluator.HandValues.Total && player3HandEvaluator.HandValues.Total > player4HandEvaluator.HandValues.Total && player3HandEvaluator.HandValues.Total > player5HandEvaluator.HandValues.Total)
                {
                    result = 2;

                }
                else if (player4HandEvaluator.HandValues.Total > player1HandEvaluator.HandValues.Total && player4HandEvaluator.HandValues.Total > player2HandEvaluator.HandValues.Total && player4HandEvaluator.HandValues.Total > player3HandEvaluator.HandValues.Total && player4HandEvaluator.HandValues.Total > player5HandEvaluator.HandValues.Total)
                {
                    result = 3;

                }
                else if (player5HandEvaluator.HandValues.Total > player1HandEvaluator.HandValues.Total && player5HandEvaluator.HandValues.Total > player2HandEvaluator.HandValues.Total && player5HandEvaluator.HandValues.Total > player3HandEvaluator.HandValues.Total && player5HandEvaluator.HandValues.Total > player4HandEvaluator.HandValues.Total)
                {
                    result = 4;

                }                //if high card is of same value check second card in players hand
                if (player1HandEvaluator.HandValues.HighCard > player2HandEvaluator.HandValues.HighCard && player1HandEvaluator.HandValues.HighCard > player3HandEvaluator.HandValues.HighCard && player1HandEvaluator.HandValues.HighCard > player4HandEvaluator.HandValues.HighCard && player1HandEvaluator.HandValues.HighCard > player5HandEvaluator.HandValues.HighCard)
                {
                    result = 0;

                }
                else if (player2HandEvaluator.HandValues.HighCard > player1HandEvaluator.HandValues.HighCard && player2HandEvaluator.HandValues.HighCard > player3HandEvaluator.HandValues.HighCard && player2HandEvaluator.HandValues.HighCard > player4HandEvaluator.HandValues.HighCard && player2HandEvaluator.HandValues.HighCard > player5HandEvaluator.HandValues.HighCard)
                {
                    result = 1;

                }
                else if (player3HandEvaluator.HandValues.HighCard > player1HandEvaluator.HandValues.HighCard && player3HandEvaluator.HandValues.HighCard > player2HandEvaluator.HandValues.HighCard && player3HandEvaluator.HandValues.HighCard > player4HandEvaluator.HandValues.HighCard && player3HandEvaluator.HandValues.HighCard > player5HandEvaluator.HandValues.HighCard)
                {
                    result = 2;

                }
                else if (player4HandEvaluator.HandValues.HighCard > player1HandEvaluator.HandValues.HighCard && player4HandEvaluator.HandValues.HighCard > player2HandEvaluator.HandValues.HighCard && player4HandEvaluator.HandValues.HighCard > player3HandEvaluator.HandValues.HighCard && player4HandEvaluator.HandValues.HighCard > player5HandEvaluator.HandValues.HighCard)
                {
                    result = 3;

                }
                else if (player5HandEvaluator.HandValues.HighCard > player1HandEvaluator.HandValues.HighCard && player5HandEvaluator.HandValues.HighCard > player2HandEvaluator.HandValues.HighCard && player5HandEvaluator.HandValues.HighCard > player3HandEvaluator.HandValues.HighCard && player5HandEvaluator.HandValues.HighCard > player4HandEvaluator.HandValues.HighCard)
                {
                    result = 4;

                }
                else
                {
                    result = 5;
                }
            }
        }
    }
}
