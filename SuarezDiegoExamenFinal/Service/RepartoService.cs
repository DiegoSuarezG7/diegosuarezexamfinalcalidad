﻿using SuarezDiegoExamenFinal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SuarezDiegoExamenFinal.Service
{
    public interface IRepartoService
    {
        public RepartoDeCartas NuevoReparto();
    }
    public class RepartoService : IRepartoService
    {
        public RepartoDeCartas NuevoReparto()
        {
            return new RepartoDeCartas();
        }
    }
}
