﻿using SuarezDiegoExamenFinal.VIewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SuarezDiegoExamenFinal.Service
{
    public interface IJuegoService
    {
        public Juego NuevoJuego();
    }

    public class JuegoService : IJuegoService
    {
        public Juego NuevoJuego()
        {
            return new Juego();
        }
    }
}
