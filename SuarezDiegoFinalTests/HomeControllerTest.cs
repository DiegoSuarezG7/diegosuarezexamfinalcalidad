﻿using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using SuarezDiegoExamenFinal.Controllers;
using SuarezDiegoExamenFinal.Models;
using SuarezDiegoExamenFinal.Service;
using SuarezDiegoExamenFinal.VIewModel;
using System;
using System.Collections.Generic;
using System.Text;
using static SuarezDiegoExamenFinal.Models.Carta;

namespace SuarezDiegoFinalTests
{
    public class HomeControllerTest
    {
        [Test]
        public void MostrarManosDeLosJuegadoresYElGanador()
        {

            var controller = new HomeController(null,null);
            var view = controller.Juego();
            Assert.IsNotNull(view);
            Assert.IsInstanceOf<ViewResult>(view);
        }

        [Test]
        public void GanaJugador3ConFourOfKind()
        {

            var rp = new RepartoDeCartas
            {
                sortedPlayer1Hand = new Carta[]
                    {
                        new Carta(Suit.Espadas, Value.As),
                        new Carta(Suit.Corazones, Value.dos),
                        new Carta(Suit.Corazones, Value.nueve),
                        new Carta(Suit.Treboles, Value.dies),
                        new Carta(Suit.Corazones, Value.once),

                    },

                sortedPlayer2Hand = new Carta[]
                    {

                       new Carta(Suit.Diamantes, Value.dos),
                       new Carta(Suit.Treboles, Value.cinco),
                       new Carta(Suit.Treboles, Value.nueve),
                       new Carta(Suit.Treboles, Value.once),
                       new Carta(Suit.Diamantes, Value.once),

                    },

                sortedPlayer3Hand = new Carta[]
                    {
                                
                       new Carta(Suit.Corazones, Value.As),
                       new Carta(Suit.Diamantes, Value.As),
                       new Carta(Suit.Espadas, Value.As),
                       new Carta(Suit.Treboles, Value.As),
                       new Carta(Suit.Diamantes, Value.cinco),

                    },
                sortedPlayer4Hand = new Carta[]
                    {

                       new Carta(Suit.Corazones, Value.As),
                       new Carta(Suit.Treboles, Value.tres),
                       new Carta(Suit.Diamantes, Value.cinco),
                       new Carta(Suit.Corazones, Value.ocho),
                       new Carta(Suit.Espadas, Value.Reina),

                            },
                sortedPlayer5Hand = new Carta[]
                    {

                       new Carta(Suit.Corazones, Value.tres),
                       new Carta(Suit.Espadas, Value.Siete),
                       new Carta(Suit.Espadas, Value.ocho),
                       new Carta(Suit.Treboles, Value.Reina),
                       new Carta(Suit.Corazones, Value.Rey),

                    },

            };

            //rp.EvaluateHands();
            //Assert.AreEqual(2, rp.result);
        }

        [Test]
        public void GanaJugador1ConEscaleraDeColor()
        {
            var rp = new RepartoDeCartas
            {
                sortedPlayer1Hand = new Carta[]
                    {
                        
                        new Carta(Suit.Diamantes, Value.tres),
                        new Carta(Suit.Diamantes, Value.cuatro),
                        new Carta(Suit.Diamantes, Value.cinco),
                        new Carta(Suit.Diamantes, Value.seis),
                        new Carta(Suit.Diamantes, Value.Siete),

                    },

                sortedPlayer2Hand = new Carta[]
                    {
                        new Carta(Suit.Corazones, Value.tres),
                        new Carta(Suit.Treboles, Value.seis),
                        new Carta(Suit.Treboles, Value.Siete),
                        new Carta(Suit.Corazones, Value.nueve),
                        new Carta(Suit.Diamantes, Value.dies),

                    },

                sortedPlayer3Hand = new Carta[]
                    {
                        new Carta(Suit.Corazones, Value.dos),
                        new Carta(Suit.Diamantes, Value.dos),
                        new Carta(Suit.Espadas, Value.seis),
                        new Carta(Suit.Diamantes, Value.ocho),
                        new Carta(Suit.Treboles, Value.dies),

                    },
                sortedPlayer4Hand = new Carta[]
                    {
                        new Carta(Suit.Espadas, Value.tres),
                        new Carta(Suit.Diamantes, Value.tres),
                        new Carta(Suit.Corazones, Value.cinco),
                        new Carta(Suit.Espadas, Value.ocho),
                        new Carta(Suit.Espadas, Value.dies),

                    },
                sortedPlayer5Hand = new Carta[]
                    {
                        new Carta(Suit.Diamantes, Value.As),
                        new Carta(Suit.Espadas, Value.As),
                        new Carta(Suit.Treboles, Value.cuatro),
                        new Carta(Suit.Diamantes, Value.nueve),
                        new Carta(Suit.Espadas, Value.once),

                    },
            };

            //rp.EvaluateHands();
            //Assert.AreEqual(0, rp.result);
        }

        [Test]
        public void GanaJugador4ConTresReyes()
        {
            var rp = new RepartoDeCartas
            {
                sortedPlayer1Hand = new Carta[]
                    {
                        new Carta(Suit.Corazones, Value.tres),
                        new Carta(Suit.Espadas, Value.Siete),
                        new Carta(Suit.Espadas, Value.ocho),
                        new Carta(Suit.Treboles, Value.Reina),
                        new Carta(Suit.Corazones, Value.Rey),

                    },

                sortedPlayer2Hand = new Carta[]
                    {
                        new Carta(Suit.Corazones, Value.tres),
                        new Carta(Suit.Treboles, Value.seis),
                        new Carta(Suit.Treboles, Value.Siete),
                        new Carta(Suit.Corazones, Value.nueve),
                        new Carta(Suit.Diamantes, Value.dies),

                    },

                sortedPlayer3Hand = new Carta[]
                    {
                        new Carta(Suit.Corazones, Value.dos),
                        new Carta(Suit.Diamantes, Value.dos),
                        new Carta(Suit.Espadas, Value.seis),
                        new Carta(Suit.Diamantes, Value.ocho),
                        new Carta(Suit.Treboles, Value.dies),

                    },
                sortedPlayer4Hand = new Carta[]
                    {
                        new Carta(Suit.Espadas, Value.Rey),
                        new Carta(Suit.Diamantes, Value.Rey),
                        new Carta(Suit.Corazones, Value.Rey),
                        new Carta(Suit.Espadas, Value.ocho),
                        new Carta(Suit.Espadas, Value.dies),

                    },
                sortedPlayer5Hand = new Carta[]
                    {
                        new Carta(Suit.Diamantes, Value.As),
                        new Carta(Suit.Espadas, Value.As),
                        new Carta(Suit.Treboles, Value.cuatro),
                        new Carta(Suit.Diamantes, Value.nueve),
                        new Carta(Suit.Espadas, Value.once),

                    },
            };

            //cs.EvaluateHands();
            //Assert.AreEqual(3, rp.result);
        }

        [Test]
        public void GanaJugador2ConDoblePar()
        {
            var rp = new RepartoDeCartas
            {
                sortedPlayer1Hand = new Carta[]
                    {

                        new Carta(Suit.Corazones, Value.tres),
                        new Carta(Suit.Treboles, Value.seis),
                        new Carta(Suit.Treboles, Value.Siete),
                        new Carta(Suit.Corazones, Value.nueve),
                        new Carta(Suit.Diamantes, Value.dies),

                    },

                sortedPlayer2Hand = new Carta[]
                    {
                        new Carta(Suit.Treboles, Value.tres),
                        new Carta(Suit.Treboles, Value.cuatro),
                        new Carta(Suit.Corazones, Value.Siete),
                        new Carta(Suit.Corazones, Value.ocho),
                        new Carta(Suit.Diamantes, Value.dies),

                    },

                sortedPlayer3Hand = new Carta[]
                    {
                        new Carta(Suit.Corazones, Value.dos),
                        new Carta(Suit.Diamantes, Value.dos),
                        new Carta(Suit.Espadas, Value.seis),
                        new Carta(Suit.Diamantes, Value.ocho),
                        new Carta(Suit.Treboles, Value.dies),

                    },
                sortedPlayer4Hand = new Carta[]
                    {
                        new Carta(Suit.Espadas, Value.tres),
                        new Carta(Suit.Diamantes, Value.tres),
                        new Carta(Suit.Corazones, Value.cinco),
                        new Carta(Suit.Espadas, Value.ocho),
                        new Carta(Suit.Espadas, Value.dies),

                    },
                sortedPlayer5Hand = new Carta[]
                    {
                        new Carta(Suit.Diamantes, Value.As),
                        new Carta(Suit.Espadas, Value.As),
                        new Carta(Suit.Treboles, Value.cuatro),
                        new Carta(Suit.Diamantes, Value.nueve),
                        new Carta(Suit.Espadas, Value.once),

                    },
            };

            //rp.EvaluateHands();
            //Assert.AreEqual(3, rp.result);
        }

        [Test]
        public void GanaJugador5ConDoblePar()
        {
            var rp = new RepartoDeCartas
            {
                sortedPlayer1Hand = new Carta[]
                    {

                        new Carta(Suit.Corazones, Value.tres),
                        new Carta(Suit.Treboles, Value.seis),
                        new Carta(Suit.Treboles, Value.Siete),
                        new Carta(Suit.Corazones, Value.nueve),
                        new Carta(Suit.Diamantes, Value.dies),

                    },

                sortedPlayer2Hand = new Carta[]
                    {
                        new Carta(Suit.Treboles, Value.tres),
                        new Carta(Suit.Diamantes, Value.cuatro),
                        new Carta(Suit.Corazones, Value.Siete),
                        new Carta(Suit.Treboles, Value.ocho),
                        new Carta(Suit.Diamantes, Value.dies),

                    },

                sortedPlayer3Hand = new Carta[]
                    {
                        new Carta(Suit.Corazones, Value.dos),
                        new Carta(Suit.Diamantes, Value.dos),
                        new Carta(Suit.Espadas, Value.seis),
                        new Carta(Suit.Diamantes, Value.ocho),
                        new Carta(Suit.Treboles, Value.dies),

                    },
                sortedPlayer4Hand = new Carta[]
                    {
                        new Carta(Suit.Espadas, Value.tres),
                        new Carta(Suit.Diamantes, Value.tres),
                        new Carta(Suit.Corazones, Value.cinco),
                        new Carta(Suit.Espadas, Value.ocho),
                        new Carta(Suit.Espadas, Value.dies),

                    },
                sortedPlayer5Hand = new Carta[]
                    {
                        new Carta(Suit.Diamantes, Value.As),
                        new Carta(Suit.Espadas, Value.As),
                        new Carta(Suit.Treboles, Value.cuatro),
                        new Carta(Suit.Diamantes, Value.nueve),
                        new Carta(Suit.Espadas, Value.once),

                    },
            };

            //rp.EvaluateHands();
            //Assert.AreEqual(4, rp.result);
        }

        [Test]
        public void EmpateJugador1y2ConDoblePar()
        {
            var rp = new RepartoDeCartas
            {
                sortedPlayer1Hand = new Carta[]
                    {

                        new Carta(Suit.Diamantes, Value.tres),
                        new Carta(Suit.Diamantes, Value.cuatro),
                        new Carta(Suit.Espadas, Value.Siete),
                        new Carta(Suit.Espadas, Value.ocho),
                        new Carta(Suit.Diamantes, Value.dies),

                    },

                sortedPlayer2Hand = new Carta[]
                    {
                        new Carta(Suit.Diamantes, Value.tres),
                        new Carta(Suit.Diamantes, Value.cuatro),
                        new Carta(Suit.Espadas, Value.Siete),
                        new Carta(Suit.Espadas, Value.ocho),
                        new Carta(Suit.Diamantes, Value.dies),

                    },

                sortedPlayer3Hand = new Carta[]
                    {
                        new Carta(Suit.Corazones, Value.dos),
                        new Carta(Suit.Diamantes, Value.dos),
                        new Carta(Suit.Espadas, Value.seis),
                        new Carta(Suit.Diamantes, Value.ocho),
                        new Carta(Suit.Treboles, Value.dies),

                    },
                sortedPlayer4Hand = new Carta[]
                    {
                        new Carta(Suit.Espadas, Value.tres),
                        new Carta(Suit.Diamantes, Value.tres),
                        new Carta(Suit.Corazones, Value.cinco),
                        new Carta(Suit.Espadas, Value.ocho),
                        new Carta(Suit.Espadas, Value.dies),

                    },
                sortedPlayer5Hand = new Carta[]
                    {
                        new Carta(Suit.Diamantes, Value.As),
                        new Carta(Suit.Espadas, Value.As),
                        new Carta(Suit.Treboles, Value.cuatro),
                        new Carta(Suit.Diamantes, Value.nueve),
                        new Carta(Suit.Espadas, Value.once),

                    },
            };

            //rp.EvaluateHands();
            //Assert.AreEqual(5, rp.result);
        }

        [Test]
        public void GanaJugador1ConFull()
        {
            var rp = new RepartoDeCartas
            {
                sortedPlayer1Hand = new Carta[]
                    {

                        new Carta(Suit.Corazones, Value.dies),
                        new Carta(Suit.Espadas, Value.dies),
                        new Carta(Suit.Corazones, Value.Rey),
                        new Carta(Suit.Espadas, Value.Rey),
                        new Carta(Suit.Treboles, Value.Rey),

                    },

                sortedPlayer2Hand = new Carta[]
                    {
                        new Carta(Suit.Treboles, Value.tres),
                        new Carta(Suit.Diamantes, Value.cuatro),
                        new Carta(Suit.Corazones, Value.Siete),
                        new Carta(Suit.Treboles, Value.ocho),
                        new Carta(Suit.Diamantes, Value.dies),

                    },

                sortedPlayer3Hand = new Carta[]
                    {
                        new Carta(Suit.Corazones, Value.dos),
                        new Carta(Suit.Diamantes, Value.dos),
                        new Carta(Suit.Espadas, Value.seis),
                        new Carta(Suit.Diamantes, Value.ocho),
                        new Carta(Suit.Treboles, Value.dies),

                    },
                sortedPlayer4Hand = new Carta[]
                    {
                        new Carta(Suit.Espadas, Value.tres),
                        new Carta(Suit.Diamantes, Value.tres),
                        new Carta(Suit.Corazones, Value.cinco),
                        new Carta(Suit.Espadas, Value.ocho),
                        new Carta(Suit.Espadas, Value.dies),

                    },
                sortedPlayer5Hand = new Carta[]
                    {
                        new Carta(Suit.Diamantes, Value.As),
                        new Carta(Suit.Espadas, Value.As),
                        new Carta(Suit.Treboles, Value.cuatro),
                        new Carta(Suit.Diamantes, Value.nueve),
                        new Carta(Suit.Espadas, Value.once),

                    },
            };

            //rp.EvaluateHands();
            //Assert.AreEqual(4, rp.result);
        }

        [Test]
        public void EmpateJugador1y2ConPar()
        {
            var rp = new RepartoDeCartas
            {
                sortedPlayer1Hand = new Carta[]
                    {

                        new Carta(Suit.Corazones, Value.As),
                        new Carta(Suit.Corazones, Value.As),
                        new Carta(Suit.Treboles, Value.cinco),
                        new Carta(Suit.Espadas, Value.Siete),
                        new Carta(Suit.Treboles, Value.ocho),

                    },

                sortedPlayer2Hand = new Carta[]
                    {
                        new Carta(Suit.Treboles, Value.As),
                        new Carta(Suit.Treboles, Value.As),
                        new Carta(Suit.Corazones, Value.cinco),
                        new Carta(Suit.Treboles, Value.seis),
                        new Carta(Suit.Diamantes, Value.ocho),

                    },

                sortedPlayer3Hand = new Carta[]
                    {
                        new Carta(Suit.Corazones, Value.dos),
                        new Carta(Suit.Diamantes, Value.dos),
                        new Carta(Suit.Espadas, Value.seis),
                        new Carta(Suit.Diamantes, Value.ocho),
                        new Carta(Suit.Treboles, Value.dies),

                    },
                sortedPlayer4Hand = new Carta[]
                    {
                        new Carta(Suit.Espadas, Value.tres),
                        new Carta(Suit.Diamantes, Value.tres),
                        new Carta(Suit.Corazones, Value.cinco),
                        new Carta(Suit.Espadas, Value.ocho),
                        new Carta(Suit.Espadas, Value.dies),

                    },
                sortedPlayer5Hand = new Carta[]
                    {
                        new Carta(Suit.Diamantes, Value.As),
                        new Carta(Suit.Espadas, Value.tres),
                        new Carta(Suit.Treboles, Value.cuatro),
                        new Carta(Suit.Diamantes, Value.nueve),
                        new Carta(Suit.Espadas, Value.once),

                    },
            };

            //rp.EvaluateHands();
            //Assert.AreEqual(5, rp.result);
        }

        [Test]
        public void GanaJugador1al2conPokerconCartaMayor()
        {
            var rp = new RepartoDeCartas
            {
                sortedPlayer1Hand = new Carta[]
                    {

                        new Carta(Suit.Corazones, Value.As),
                        new Carta(Suit.Diamantes, Value.As),
                        new Carta(Suit.Treboles, Value.As),
                        new Carta(Suit.Espadas, Value.As),
                        new Carta(Suit.Treboles, Value.ocho),

                    },

                sortedPlayer2Hand = new Carta[]
                    {
                        new Carta(Suit.Corazones, Value.As),
                        new Carta(Suit.Diamantes, Value.As),
                        new Carta(Suit.Treboles, Value.As),
                        new Carta(Suit.Espadas, Value.As),
                        new Carta(Suit.Treboles, Value.tres),

                    },

                sortedPlayer3Hand = new Carta[]
                    {
                        new Carta(Suit.Corazones, Value.dos),
                        new Carta(Suit.Diamantes, Value.dos),
                        new Carta(Suit.Espadas, Value.seis),
                        new Carta(Suit.Diamantes, Value.ocho),
                        new Carta(Suit.Treboles, Value.dies),

                    },
                sortedPlayer4Hand = new Carta[]
                    {
                        new Carta(Suit.Espadas, Value.tres),
                        new Carta(Suit.Diamantes, Value.tres),
                        new Carta(Suit.Corazones, Value.cinco),
                        new Carta(Suit.Espadas, Value.ocho),
                        new Carta(Suit.Espadas, Value.dies),

                    },
                sortedPlayer5Hand = new Carta[]
                    {
                        new Carta(Suit.Diamantes, Value.As),
                        new Carta(Suit.Espadas, Value.tres),
                        new Carta(Suit.Treboles, Value.cuatro),
                        new Carta(Suit.Diamantes, Value.nueve),
                        new Carta(Suit.Espadas, Value.once),

                    },
            };

            //rp.EvaluateHands();
            //Assert.AreEqual(0, rp.result);
        }

        [Test]
        public void GanaJugador1al2conFullMasAlto()
        {
            var rp = new RepartoDeCartas
            {
                sortedPlayer1Hand = new Carta[]
                    {

                        new Carta(Suit.Corazones, Value.nueve),
                        new Carta(Suit.Espadas, Value.nueve),
                        new Carta(Suit.Corazones, Value.Rey),
                        new Carta(Suit.Espadas, Value.Rey),
                        new Carta(Suit.Treboles, Value.Rey),

                    },

                sortedPlayer2Hand = new Carta[]
                    {
                        new Carta(Suit.Corazones, Value.dies),
                        new Carta(Suit.Espadas, Value.dies),
                        new Carta(Suit.Corazones, Value.Rey),
                        new Carta(Suit.Espadas, Value.Rey),
                        new Carta(Suit.Treboles, Value.Rey),

                    },

                sortedPlayer3Hand = new Carta[]
                    {
                        new Carta(Suit.Corazones, Value.dos),
                        new Carta(Suit.Diamantes, Value.dos),
                        new Carta(Suit.Espadas, Value.seis),
                        new Carta(Suit.Diamantes, Value.ocho),
                        new Carta(Suit.Treboles, Value.dies),

                    },
                sortedPlayer4Hand = new Carta[]
                    {
                        new Carta(Suit.Espadas, Value.tres),
                        new Carta(Suit.Diamantes, Value.tres),
                        new Carta(Suit.Corazones, Value.cinco),
                        new Carta(Suit.Espadas, Value.ocho),
                        new Carta(Suit.Espadas, Value.dies),

                    },
                sortedPlayer5Hand = new Carta[]
                    {
                        new Carta(Suit.Diamantes, Value.As),
                        new Carta(Suit.Espadas, Value.tres),
                        new Carta(Suit.Treboles, Value.cuatro),
                        new Carta(Suit.Diamantes, Value.nueve),
                        new Carta(Suit.Espadas, Value.once),

                    },
            };

            //rp.EvaluateHands();
            //Assert.AreEqual(0, rp.result);
        }
    }
}
